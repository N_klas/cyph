import Vue from "vue";
import Vuex from "vuex";
import { AST_SymbolExportForeign } from "terser";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    packages: [],
    packageCount: 0,
    errors: ""
  },
  mutations: {
    refreshPackages(state) {
      state.errors = ""
      if (
        localStorage.getItem("repositories") != undefined &&
        localStorage.repositories !== ""
      ) {
        state.packages = new Array()
        JSON.parse(localStorage.getItem("repositories")).forEach(element => {
          axios
            .get("https://api.s0n1c.org/cydia?url=" + element)
            .then(response => {
              if (response.data.error) {
                console.error(response.data.error)
                state.errors = state.errors + element + ": " + response.data.error
                return;
              }
              var result = Object.keys(response.data.data.packages).map(function (key) {
                var p = response.data.data.packages[key];
                state.packageCount++;
                if (p.length > 1) {
                  return p[p.length - 1]
                }
                return p;
              })
              state.packages.push(result)
            }).catch(error => {
              console.error(error.response.data.error)
              state.errors = state.errors + element + ":\n" + error.response.data.error + "\n"
            });
        });
      }
    }
  }
  ,
  actions: {

  }
});
